package pl.edu.agh.toik.automaton.board;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static pl.edu.agh.toik.automaton.board.Simple2DBooleanMatrix.*;

// @formatter:off
public class BoardFragmentTest {

    @Test
    public void access_outside_board_and_borderlands_should_not_be_allowed() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        try {
            boardFragment.get(-10, -10);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(1, -10);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(-10, 1);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(10, 1);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(10, 10);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(1, 10);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(10, -10);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
        try {
            boardFragment.get(-10, 10);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void operations_on_board_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(0, 0, true);
        boardFragment.internalUnsafeSet(1, 1, true);
        boardFragment.internalUnsafeSet(2, 2, true);

        assertThat(boardFragment.get(0, 0)).isTrue();
        assertThat(boardFragment.get(1, 1)).isTrue();
        assertThat(boardFragment.get(1, 1)).isTrue();

        assertThat(boardFragment.asText()).isEqualTo(
                "x  \n" +
                " x \n" +
                "  x\n");
    }

    @Test
    public void operations_on_north_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(0, 3, true);
        boardFragment.internalUnsafeSet(2, 4, true);

        assertThat(boardFragment.get(0, 3)).isTrue();
        assertThat(boardFragment.get(1, 3)).isFalse();
        assertThat(boardFragment.get(2, 3)).isFalse();
        assertThat(boardFragment.get(3, 3)).isFalse();
        assertThat(boardFragment.get(0, 4)).isFalse();
        assertThat(boardFragment.get(1, 4)).isFalse();
        assertThat(boardFragment.get(2, 4)).isTrue();
        assertThat(boardFragment.get(3, 4)).isFalse();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_north_east_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(3, 3, true);
        boardFragment.internalUnsafeSet(4, 4, true);

        assertThat(boardFragment.get(3, 3)).isTrue();
        assertThat(boardFragment.get(3, 4)).isFalse();
        assertThat(boardFragment.get(4, 3)).isFalse();
        assertThat(boardFragment.get(4, 4)).isTrue();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_east_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(3, 0, true);
        boardFragment.internalUnsafeSet(4, 1, true);

        assertThat(boardFragment.get(3, 0)).isTrue();
        assertThat(boardFragment.get(3, 1)).isFalse();
        assertThat(boardFragment.get(3, 2)).isFalse();
        assertThat(boardFragment.get(4, 0)).isFalse();
        assertThat(boardFragment.get(4, 1)).isTrue();
        assertThat(boardFragment.get(4, 2)).isFalse();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_south_east_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(3, -1, true);
        boardFragment.internalUnsafeSet(4, -2, true);

        assertThat(boardFragment.get(3, -1)).isTrue();
        assertThat(boardFragment.get(3, -2)).isFalse();
        assertThat(boardFragment.get(4, -1)).isFalse();
        assertThat(boardFragment.get(4, -2)).isTrue();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_south_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(0, -1, true);
        boardFragment.internalUnsafeSet(2, -2, true);

        assertThat(boardFragment.get(0, -1)).isTrue();
        assertThat(boardFragment.get(1, -1)).isFalse();
        assertThat(boardFragment.get(2, -1)).isFalse();
        assertThat(boardFragment.get(3, -1)).isFalse();
        assertThat(boardFragment.get(0, -2)).isFalse();
        assertThat(boardFragment.get(1, -2)).isFalse();
        assertThat(boardFragment.get(2, -2)).isTrue();
        assertThat(boardFragment.get(3, -2)).isFalse();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_south_west_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(-1, -1, true);
        boardFragment.internalUnsafeSet(-2, -2, true);

        assertThat(boardFragment.get(-1, -1)).isTrue();
        assertThat(boardFragment.get(-1, -2)).isFalse();
        assertThat(boardFragment.get(-2, -1)).isFalse();
        assertThat(boardFragment.get(-2, -2)).isTrue();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_west_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(-1, 0, true);
        boardFragment.internalUnsafeSet(-2, 1, true);

        assertThat(boardFragment.get(-1, 0)).isTrue();
        assertThat(boardFragment.get(-1, 1)).isFalse();
        assertThat(boardFragment.get(-1, 2)).isFalse();
        assertThat(boardFragment.get(-2, 0)).isFalse();
        assertThat(boardFragment.get(-2, 1)).isTrue();
        assertThat(boardFragment.get(-2, 2)).isFalse();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test
    public void operations_on_north_west_borderland_should_work() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.internalUnsafeSet(-1, 3, true);
        boardFragment.internalUnsafeSet(-2, 4, true);

        assertThat(boardFragment.get(-1, 3)).isTrue();
        assertThat(boardFragment.get(-1, 4)).isFalse();
        assertThat(boardFragment.get(-2, 3)).isFalse();
        assertThat(boardFragment.get(-2, 4)).isTrue();

        assertThat(boardFragment.asText()).isEqualTo(
                "   \n" +
                "   \n" +
                "   \n");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_not_allow_to_writing_on_borderland() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);

        boardFragment.set(-1, -1, true);
    }

    @Test
    public void should_export_north_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.N);
        assertThat(border.getHeight()).isEqualTo(1);
        assertThat(border.getWidth()).isEqualTo(4);
        assertThat(border.get(0, 0)).isTrue();
        assertThat(border.get(1, 0)).isFalse();
        assertThat(border.get(2, 0)).isFalse();
        assertThat(border.get(3, 0)).isTrue();
    }

    @Test
    public void should_export_north_east_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.NE);
        assertThat(border.getHeight()).isEqualTo(1);
        assertThat(border.getWidth()).isEqualTo(1);
        assertThat(border.get(0, 0)).isTrue();
    }

    @Test
    public void should_export_east_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.E);
        assertThat(border.getHeight()).isEqualTo(4);
        assertThat(border.getWidth()).isEqualTo(1);
        assertThat(border.get(0, 0)).isTrue();
        assertThat(border.get(0, 1)).isFalse();
        assertThat(border.get(0, 2)).isFalse();
        assertThat(border.get(0, 3)).isTrue();
    }

    @Test
    public void should_export_south_east_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.SE);
        assertThat(border.getHeight()).isEqualTo(1);
        assertThat(border.getWidth()).isEqualTo(1);
        assertThat(border.get(0, 0)).isTrue();
    }

    @Test
    public void should_export_south_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.S);
        assertThat(border.getHeight()).isEqualTo(1);
        assertThat(border.getWidth()).isEqualTo(4);
        assertThat(border.get(0, 0)).isTrue();
        assertThat(border.get(1, 0)).isFalse();
        assertThat(border.get(2, 0)).isFalse();
        assertThat(border.get(3, 0)).isTrue();
    }

    @Test
    public void should_export_south_west_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.SW);
        assertThat(border.getHeight()).isEqualTo(1);
        assertThat(border.getWidth()).isEqualTo(1);
        assertThat(border.get(0, 0)).isTrue();
    }

    @Test
    public void should_export_west_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.W);
        assertThat(border.getHeight()).isEqualTo(4);
        assertThat(border.getWidth()).isEqualTo(1);
        assertThat(border.get(0, 0)).isTrue();
        assertThat(border.get(1, 0)).isFalse();
        assertThat(border.get(2, 0)).isFalse();
        assertThat(border.get(3, 0)).isTrue();
    }

    @Test
    public void should_export_north_west_border_correctly() {
        BoardFragment boardFragment = sampleBoard();

        Simple2DBooleanMatrix border = boardFragment.getBorder(Direction.NW);
        assertThat(border.getHeight()).isEqualTo(1);
        assertThat(border.getWidth()).isEqualTo(1);
        assertThat(border.get(0, 0)).isTrue();
    }

    @Test
    public void should_update_north_border_land_correctly() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);
        boardFragment.updateBorderLand(Direction.N, matrix(3, 2,
                x, o, x,
                o, x, o));

        assertThat(boardFragment.get(0, -2)).isEqualTo(x);
        assertThat(boardFragment.get(1, -2)).isEqualTo(o);
        assertThat(boardFragment.get(2, -2)).isEqualTo(x);
        assertThat(boardFragment.get(0, -1)).isEqualTo(o);
        assertThat(boardFragment.get(1, -1)).isEqualTo(x);
        assertThat(boardFragment.get(2, -1)).isEqualTo(o);
    }

    @Test
    public void should_update_north_east_border_land_correctly() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);
        boardFragment.updateBorderLand(Direction.NE, matrix(2, 2,
                x, o,
                o, x));

        assertThat(boardFragment.get(4, -2)).isEqualTo(o);
        assertThat(boardFragment.get(3, -2)).isEqualTo(x);
        assertThat(boardFragment.get(3, -1)).isEqualTo(o);
        assertThat(boardFragment.get(4, -1)).isEqualTo(x);
    }

    @Test
    public void should_update_east_border_land_correctly() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);
        boardFragment.updateBorderLand(Direction.E, matrix(2, 3,
                x, o,
                o, x,
                x, o));

        assertThat(boardFragment.get(3, 0)).isEqualTo(x);
        assertThat(boardFragment.get(4, 0)).isEqualTo(o);
        assertThat(boardFragment.get(3, 1)).isEqualTo(o);
        assertThat(boardFragment.get(4, 1)).isEqualTo(x);
        assertThat(boardFragment.get(3, 2)).isEqualTo(x);
        assertThat(boardFragment.get(4, 2)).isEqualTo(o);
    }

    @Test
    public void should_update_south_east_border_land_correctly() {
        BoardFragment boardFragment = new BoardFragment(3, 3, 2);
        boardFragment.updateBorderLand(Direction.SE, matrix(2, 2,
                x, o,
                o, x));

        assertThat(boardFragment.get(3, 3)).isEqualTo(x);
        assertThat(boardFragment.get(4, 3)).isEqualTo(o);
        assertThat(boardFragment.get(3, 4)).isEqualTo(o);
        assertThat(boardFragment.get(4, 4)).isEqualTo(x);
    }


    private BoardFragment sampleBoard() {
        BoardFragment boardFragment = new BoardFragment(4, 4, 1);
        boardFragment.set(0, 0, true);
        boardFragment.set(1, 1, true);
        boardFragment.set(2, 2, true);
        boardFragment.set(3, 3, true);
        boardFragment.set(3, 0, true);
        boardFragment.set(2, 1, true);
        boardFragment.set(1, 2, true);
        boardFragment.set(0, 3, true);

        assertThat(boardFragment.asText()).isEqualTo(
            "x  x\n" +
            " xx \n" +
            " xx \n" +
            "x  x\n");return boardFragment;
    }

}
