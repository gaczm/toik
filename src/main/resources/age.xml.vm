<?xml version="1.0" encoding="UTF-8" ?>
<configuration xmlns="http://age.iisg.agh.edu.pl/schema/age"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:schemaLocation="http://age.iisg.agh.edu.pl/schema/age http://age.iisg.agh.edu.pl/schema/age/age.xsd">

    <component class="org.jage.address.node.DefaultNodeAddressSupplier"/>
    <component class="org.jage.communication.LoopbackCommunicationService"/>

    <component name="configurationService" class="org.jage.lifecycle.DefaultConfigurationService"/>

    <component name="configuration" class="pl.edu.agh.toik.automaton.simulation.Configuration">
        <constructor-arg>
            <map name="configuration">
                <entry key="numberOfFragments" value="${numberOfFragments}"/>
                <entry key="boardSize" value="${boardSize}"/>
                <entry key="synchronizationFrequency" value="${synchronizationFrequency}"/>
                <entry key="borderSize" value="${borderSize}"/>
                <entry key="numberOfIterations" value="${numberOfIterations}"/>
            </map>
        </constructor-arg>
    </component>

    <component name="workplaceManager" class="org.jage.workplace.manager.DefaultWorkplaceManager">
        <block name="workplaceManagerComponents">
            <component name="defaultAgentAddressSupplier" class="org.jage.address.agent.DefaultAgentAddressSupplier"/>
            <component name="comparator" class="org.jage.action.ordering.DefaultActionComparator" isSingleton="false"/>
        </block>

        <block name="workplaceComponents">
            <component name="aggregateActionService" class="org.jage.agent.AggregateActionService" isSingleton="false"/>
            <component name="aggregateQueryService" class="org.jage.agent.AggregateQueryService" isSingleton="false"/>
            <component name="aggregateMessagingService" class="org.jage.agent.AggregateMessagingService" isSingleton="false"/>
        </block>

        <property name="workplaces">
            <list>
                <block name="workplaces">

                    <agent name="visualisingAgent" class="pl.edu.agh.toik.automaton.simulation.VisualisingAgent">
                        <component class="org.jage.address.agent.DefaultAgentAddressSupplier">
                            <constructor-arg name="nameTemplate" value="visualisingAgent"/>
                        </component>
                    </agent>

                    #set( $numberOfFragmentsMinusOne = $numberOfFragments - 1)
                    #foreach( $fragmentIndex in [0..$numberOfFragmentsMinusOne] )
                        <agent name="boardFragment-$fragmentIndex" class="pl.edu.agh.toik.automaton.simulation.BoardFragmentOwningAgent">
                            <component class="org.jage.address.agent.DefaultAgentAddressSupplier">
                                <constructor-arg name="nameTemplate" value="boardFragment-$fragmentIndex"/>
                            </component>
                            <list name="agents">
                                <agent name="simpleAggregate" class="pl.edu.agh.toik.automaton.simulation.AntAggregate">
                                    <component class="org.jage.address.agent.DefaultAgentAddressSupplier">
                                        <constructor-arg name="nameTemplate" value="ants-$fragmentIndex"/>
                                    </component>
                                    <property name="agents">
                                        <list>
                                            #set( $numberOfAntsMinusOne = $numberOfAnts - 1)
                                            #foreach( $antIndex in [0..$numberOfAntsMinusOne] )
                                                <agent name="ant-$antIndex" class="pl.edu.agh.toik.automaton.simulation.AntAgent">
                                                    <component class="org.jage.address.agent.DefaultAgentAddressSupplier">
                                                        <constructor-arg name="nameTemplate" value="ant-$antIndex"/>
                                                    </component>
                                                </agent>
                                            #end
                                        </list>
                                    </property>
                                </agent>
                            </list>
                            <property name="agents" ref="agents" />
                        </agent>
                    #end
                </block>
            </list>
        </property>

        <block name="stopCondition">
            <component name="stopCondition" class="org.jage.workplace.FixedStepCountStopCondition">
                <constructor-arg type="Long" value="${numberOfIterations}"/>
            </component>
        </block>
    </component>
</configuration>
