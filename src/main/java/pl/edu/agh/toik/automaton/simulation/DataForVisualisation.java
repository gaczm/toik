package pl.edu.agh.toik.automaton.simulation;

import pl.edu.agh.toik.automaton.board.Simple2DBooleanMatrix;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class DataForVisualisation implements Serializable {
    private static final long serialVersionUID = -6508032924054720814L;

    private final Simple2DBooleanMatrix board;
    private final List<AntDescriptor> ants;
    private final long step;
    private final int borderSize;

    public DataForVisualisation(Simple2DBooleanMatrix board, List<AntDescriptor> ants, long step, int borderSize) {
        this.board = board;
        this.ants = ants;
        this.step = step;
        this.borderSize = borderSize;
    }

    public Simple2DBooleanMatrix getBoard() {
        return board;
    }

    public List<AntDescriptor> getAnts() {
        return Collections.unmodifiableList(ants);
    }

    public long getStep() {
        return step;
    }

    public int getBorderSize() {
        return borderSize;
    }
}
