package pl.edu.agh.toik.automaton.board;

import java.io.Serializable;

import static org.apache.commons.lang3.Validate.isTrue;

public class Simple2DBooleanMatrix implements Serializable {

    private static final long serialVersionUID = 5921745024756318700L;

    public static final boolean x = true;
    public static final boolean o = false;

    public static Simple2DBooleanMatrix matrix(int width, int height, boolean... values) {
        isTrue(width * height == values.length);
        Simple2DBooleanMatrix matrix = new Simple2DBooleanMatrix(width, height);
        for (int i = 0; i < values.length; i++) {
            matrix.set(i % width, i / width, values[i]);
        }
        return matrix;
    }

    private final int width;
    private final int height;
    private final boolean[] data;

    public Simple2DBooleanMatrix(int width, int height) {
        this.width = width;
        this.height = height;
        this.data = new boolean[width * height];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean get(int x, int y) {
        return data[width * y + x];
    }

    public void set(int x, int y, boolean value) {
        data[width * y + x] = value;
    }

    @Override
    public String toString() {
        return "Simple2DBooleanMatrix " + height + "x" + width;
    }
}
