package pl.edu.agh.toik.automaton;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class AgeConfigurationGenerator {

    public static Path generateAgeConfigurationXML(Map<String, Object> configuration) {
        try {
            VelocityContext ctx = prepareContext(configuration);
            String template = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("age.xml.vm"));
            return renderTemplate(template, ctx);
        } catch (IOException e) {
            throw new RuntimeException("Cannot load template", e);
        }
    }

    private static VelocityContext prepareContext(Map<String, Object> configuration) {
        Velocity.init();
        VelocityContext ctx = new VelocityContext();
        for (Map.Entry<String, Object> entry : configuration.entrySet()) {
            ctx.put(entry.getKey(), entry.getValue());
        }
        return ctx;
    }

    private static Path renderTemplate(String template, VelocityContext ctx) throws IOException {
        Path tempFile = Files.createTempFile("age", ".xml");

        try (Writer writer = Files.newBufferedWriter(tempFile, Charset.forName("utf-8"))) {
            Velocity.evaluate(ctx, writer, "age.xml", template);
        }

        return tempFile.toAbsolutePath();
    }

}
