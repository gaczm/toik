package pl.edu.agh.toik.automaton.simulation;

import org.jage.address.agent.AgentAddress;
import org.jage.address.agent.AgentAddressSupplier;
import org.jage.agent.IAgent;
import org.jage.agent.SimpleAggregate;
import org.jage.platform.component.exception.ComponentException;
import pl.edu.agh.toik.automaton.board.BoardFragment;
import pl.edu.agh.toik.automaton.board.Direction;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

public class AntAggregate extends SimpleAggregate {

    private static final long serialVersionUID = 6601075582904239511L;
    private Configuration configuration;

    @Inject
    public AntAggregate(Configuration configuration, AgentAddressSupplier supplier) {
        super(supplier);
        this.configuration = configuration;
    }

    @Override
    public void init() throws ComponentException {
        super.init();
        System.out.println("Initializing aggregate");
    }

    private BoardFragmentOwningAgent getParent() {
        return (BoardFragmentOwningAgent) getAgentEnvironment();
    }

    public BoardFragment getBoardFragment() {
        return getParent().getBoardFragment();
    }

    public AgentAddress getAddressOfAggregateAt(Direction direction) {
        return getParent().getAddressOfAggregateAt(direction);
    }

    public AntAggregate getAggregateAt(Direction direction) {
        return getParent().getAggregateAt(direction);
    }

    @Override
    public void step() {
        super.step();

        handleMigrations();
    }

    private void handleMigrations() {
        Collection<IAgent> values = new ArrayList<IAgent>(agents.values());
        for (IAgent agent : values) {
            AntAgent ant = (AntAgent) agent;
            if (ant.getAntDescriptor().getState().equals(AntDescriptor.State.ABOUT_TO_MIGRATE)) {
                remove(ant);
                AntAggregate destination = getParent().getAggregateAt(getDirectionForCoordinates(ant.getAntDescriptor().getCoordinates()));
                ant.migrate(destination);
            }
        }
    }

    private Direction getDirectionForCoordinates(Coordinates coordinates) {
        Direction direction = null;
        BoardFragment boardFragment = getParent().getBoardFragment();
        if (coordinates.getX() < 0) {
            if (coordinates.getY() < 0) {
                direction = Direction.NW;
            } else if (coordinates.getY() >= boardFragment.getHeight()) {
                direction = Direction.SW;
            } else {
                direction = Direction.W;
            }
        } else if (coordinates.getX() >= boardFragment.getWidth()) {
            if (coordinates.getY() < 0) {
                direction = Direction.NE;
            } else if (coordinates.getY() >= boardFragment.getHeight()) {
                direction = Direction.SE;
            } else {
                direction = Direction.E;
            }
        } else {
            if (coordinates.getY() < 0) {
                direction = Direction.N;
            } else if (coordinates.getY() >= boardFragment.getHeight()) {
                direction = Direction.S;
            }
        }
        return direction;
    }
}
