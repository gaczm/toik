package pl.edu.agh.toik.automaton.board;

import com.google.common.annotations.VisibleForTesting;

import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.validator.GenericValidator.isInRange;

public class BoardFragment {

    private final int borderSize;
    private final int width, height;
    private final Simple2DBooleanMatrix data;

    public BoardFragment(int width, int height, int borderSize) {
        this.width = width;
        this.height = height;
        this.borderSize = borderSize;
        this.data = new Simple2DBooleanMatrix(width + 2 * borderSize, height + 2 * borderSize);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getBorderSize() {
        return borderSize;
    }

    public void set(int x, int y, boolean value) {
        isTrue(isInRange(x, 0, width - 1), "Writing outside of the board is not allowed");
        isTrue(isInRange(y, 0, height - 1), "Writing outside of the board is not allowed");
        internalUnsafeSet(x, y, value);
    }

    /**
     * It is only for testing, don't use it! It doesn't validate input arguments, may cause IndexArrayOutOfBoundException and allow writing on borderlands
     */
    @VisibleForTesting
    void internalUnsafeSet(int x, int y, boolean value) {
        data.set(x + borderSize, y + borderSize, value);
    }

    public boolean get(int x, int y) {
        isTrue(isInRange(x, -borderSize, width + borderSize - 1));
        isTrue(isInRange(y, -borderSize, height + borderSize - 1));
        return data.get(x + borderSize, y + borderSize);
    }

    private int calculateIndex(int x, int y) {
        return (width + 2 * borderSize) * (y + borderSize) + (x + borderSize);
    }

    /**
     * Heavy, so it's not defined as toString method
     */
    public String asText() {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < getHeight(); ++y) {
            for (int x = 0; x < getWidth(); ++x) {
                sb.append(get(x, y) ? 'x' : ' ');
            }
            sb.append('\n');
        }

        return sb.toString();
    }

    /**
     * Returns border taken from THIS board
     */
    public Simple2DBooleanMatrix getBorder(Direction direction) {
        return direction.getBorder(this);
    }

    public void updateBorderLand(Direction direction, Simple2DBooleanMatrix borderLand) {
        direction.updateBorderLand(this, borderLand);
    }

    public Simple2DBooleanMatrix getCentralPart() {
        Simple2DBooleanMatrix boardCopy = new Simple2DBooleanMatrix(width, height);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                boardCopy.set(x, y, get(x, y));
            }
        }
        return boardCopy;
    }

    @Override
    public String toString() {
        return "BoardFragment " + width + "x" + height;
    }
}
