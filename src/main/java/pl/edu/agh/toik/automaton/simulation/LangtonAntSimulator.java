package pl.edu.agh.toik.automaton.simulation;

import pl.edu.agh.toik.automaton.board.BoardFragment;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

public class LangtonAntSimulator implements Serializable, Simulation {

    private static final long serialVersionUID = 5356717083958404246L;

    private int xChange, yChange;

    public LangtonAntSimulator() {
        Random random = new Random();
        xChange = random.nextBoolean() ? 1 : -1;
        yChange = random.nextBoolean() ? 1 : -1;
    }

    @Override
    public Coordinates doStep(Coordinates antCoordinates, BoardFragment boardFragment, List<Coordinates> otherAntsCoordinateses) {
//        if (!antDescriptor.getState().equals(AntDescriptor.State.LIVING))
//            return;

        if (antCoordinates.getX() < boardFragment.getWidth() && antCoordinates.getX() >= 0 &&
                antCoordinates.getY() < boardFragment.getHeight() && antCoordinates.getY() >= 0) {
            if (boardFragment.get(antCoordinates.getX(), antCoordinates.getY())) {
                //turn left
                if (xChange == 0) { //if moving up or down
                    xChange = yChange;
                    yChange = 0;
                } else { //if moving left or right
                    yChange = -xChange;
                    xChange = 0;
                }
            } else {
                //turn right
                if (xChange == 0) { //if moving up or down
                    xChange = -yChange;
                    yChange = 0;
                } else { //if moving left or right
                    yChange = xChange;
                    xChange = 0;
                }
            }
        }

//            boardFragment.set(antDescriptor.getPositionX(), antDescriptor.getPositionY(), !boardFragment.get(antDescriptor.getPositionX(), antDescriptor.getPositionY()));
//            antDescriptor = antDescriptor.move(xChange, yChange);
        return new Coordinates(antCoordinates.getX() + xChange, antCoordinates.getY() + yChange);

//            if (antDescriptor.getPositionX() < 0 || antDescriptor.getPositionX() >= boardFragment.getWidth()
//                    || antDescriptor.getPositionY() < 0 || antDescriptor.getPositionY() >= boardFragment.getHeight()) {
//                antDescriptor = antDescriptor.changeState(AntDescriptor.State.ABOUT_TO_MIGRATE);
//            }
//        }
    }

}