package pl.edu.agh.toik.automaton.simulation;

import org.jage.address.agent.AgentAddress;
import org.jage.communication.message.IMessage;

public interface MessageReceiver<T> {

    void receive(AgentAddress senderAddress, long step, T payload);

    boolean canHandle(IMessage message);

}
