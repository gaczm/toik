package pl.edu.agh.toik.automaton.simulation;

import java.io.Serializable;

public class AntDescriptor implements Serializable {

    private static final long serialVersionUID = 6256664008924532145L;

    public enum State {LIVING, DEAD, ABOUT_TO_MIGRATE}

    private final int positionX;
    private final int positionY;
    private final State state;

    public AntDescriptor(int positionX, int positionY) {
        this(positionX, positionY, State.LIVING);
    }

    public AntDescriptor(int positionX, int positionY, State state) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.state = state;
    }

    public Coordinates getCoordinates() {
        return new Coordinates(positionX, positionY);
    }

    public State getState() {
        return state;
    }

    public AntDescriptor changeState(State state) {
        return new AntDescriptor(positionX, positionY, state);
    }
}
