package pl.edu.agh.toik.automaton.board;

import pl.edu.agh.toik.automaton.simulation.Coordinates;

public enum Direction {
    N {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX(), other.getY() - 1);
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getWidth(), boardFragment.getBorderSize(), 0, 0);
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, 0, -borderLand.getHeight());
        }

        @Override
        public Direction getOpposite() {
            return S;
        }
    }, NE {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX() + 1, other.getY() - 1);
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getBorderSize(), boardFragment.getBorderSize(), boardFragment.getWidth() - boardFragment.getBorderSize(), 0);
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, boardFragment.getWidth(), -borderLand.getHeight());
        }

        @Override
        public Direction getOpposite() {
            return SW;
        }
    }, E {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX() + 1, other.getY());
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getBorderSize(), boardFragment.getHeight(), boardFragment.getWidth() - boardFragment.getBorderSize(), 0);
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, boardFragment.getWidth(), 0);
        }

        @Override
        public Direction getOpposite() {
            return W;
        }
    }, SE {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX() + 1, other.getY() + 1);
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getBorderSize(), boardFragment.getBorderSize(), boardFragment.getWidth() - boardFragment.getBorderSize(), boardFragment.getHeight() - boardFragment.getBorderSize());
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, boardFragment.getWidth(), boardFragment.getHeight());
        }

        @Override
        public Direction getOpposite() {
            return NW;
        }
    }, S {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX(), other.getY() + 1);
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getWidth(), boardFragment.getBorderSize(), 0, boardFragment.getHeight() - boardFragment.getBorderSize());
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, 0, boardFragment.getHeight());
        }

        @Override
        public Direction getOpposite() {
            return N;
        }
    }, SW {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX() - 1, other.getY() + 1);
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getBorderSize(), boardFragment.getBorderSize(), 0, boardFragment.getHeight() - boardFragment.getBorderSize());
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, -borderLand.getWidth(), boardFragment.getHeight());
        }

        @Override
        public Direction getOpposite() {
            return NE;
        }
    }, W {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX() - 1, other.getY());
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getBorderSize(), boardFragment.getHeight(), 0, 0);
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, -borderLand.getWidth(), 0);
        }

        @Override
        public Direction getOpposite() {
            return E;
        }
    }, NW {
        @Override
        public Coordinates getNeighbourCoordinates(Coordinates other) {
            return new Coordinates(other.getX() - 1, other.getY() - 1);
        }

        @Override
        Simple2DBooleanMatrix getBorder(BoardFragment boardFragment) {
            return getBorder(boardFragment, boardFragment.getBorderSize(), boardFragment.getBorderSize(), 0, 0);
        }

        @Override
        void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand) {
            updateBorderLand(boardFragment, borderLand, -borderLand.getWidth(), -borderLand.getHeight());
        }

        @Override
        public Direction getOpposite() {
            return SE;
        }
    };

    public abstract Coordinates getNeighbourCoordinates(Coordinates other);

    abstract Simple2DBooleanMatrix getBorder(BoardFragment boardFragment);

    protected Simple2DBooleanMatrix getBorder(BoardFragment boardFragment, int width, int height, int offsetX, int offsetY) {
        Simple2DBooleanMatrix border = new Simple2DBooleanMatrix(width, height);
        for (int x = 0; x < width; ++x)
            for (int y = 0; y < height; ++y)
                border.set(x, y, boardFragment.get(x + offsetX, y + offsetY));
        return border;
    }

    abstract void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand);

    protected void updateBorderLand(BoardFragment boardFragment, Simple2DBooleanMatrix borderLand, int offsetX, int offsetY) {
        for (int x = 0; x < borderLand.getWidth(); ++x) {
            for (int y = 0; y < borderLand.getHeight(); ++y) {
                boardFragment.internalUnsafeSet(x + offsetX, y + offsetY, borderLand.get(x, y));
            }
        }
    }

    public abstract Direction getOpposite();
}
