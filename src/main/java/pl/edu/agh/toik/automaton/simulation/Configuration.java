package pl.edu.agh.toik.automaton.simulation;

import com.google.common.math.IntMath;
import org.apache.commons.collections.MapUtils;

import javax.inject.Inject;
import java.math.RoundingMode;
import java.util.Map;

/**
 * Simple wrapper for map with configuration
 */
public class Configuration {

    private final int boardSize;
    private final int numberOfIterations;
    private final int synchronizationFrequency;
    private final int visualisationFrequency;
    private final int numberOfFragments;
    private final int fragmentsInRow;
    private final int borderSize;

    public Configuration(Map<String, String> mapWithConfiguration) {
        boardSize = MapUtils.getInteger(mapWithConfiguration, "boardSize");
        numberOfIterations = MapUtils.getInteger(mapWithConfiguration, "numberOfIterations");
        synchronizationFrequency = MapUtils.getInteger(mapWithConfiguration, "synchronizationFrequency");
        visualisationFrequency = MapUtils.getInteger(mapWithConfiguration, "synchronizationFrequency");
        numberOfFragments = MapUtils.getInteger(mapWithConfiguration, "numberOfFragments");
        fragmentsInRow = IntMath.sqrt(numberOfFragments, RoundingMode.UNNECESSARY);
        borderSize = MapUtils.getInteger(mapWithConfiguration, "borderSize");
    }

    public int getBoardSize() {
        return boardSize;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public int getSynchronizationFrequency() {
        return synchronizationFrequency;
    }

    public int getVisualisationFrequency() {
        return visualisationFrequency;
    }

    public int getNumberOfFragments() {
        return numberOfFragments;
    }

    public int getFragmentsInRow() {
        return fragmentsInRow;
    }

    public int getBorderSize() {
        return borderSize;
    }

    @Override
    public String toString() {
        return "boardSize: " + boardSize + "\n" +
                "numberOfIterations: " + numberOfIterations + "\n" +
                "synchronizationFrequency: " + synchronizationFrequency + "\n" +
                "visualisationFrequency: " + visualisationFrequency + "\n" +
                "numberOfFragments: " + numberOfFragments + "\n" +
                "fragmentsInRow: " + fragmentsInRow + "\n" +
                "borderSize: " + borderSize;
    }
}
