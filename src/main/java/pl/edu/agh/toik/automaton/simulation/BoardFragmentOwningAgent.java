package pl.edu.agh.toik.automaton.simulation;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import org.jage.address.agent.AgentAddress;
import org.jage.address.agent.AgentAddressSupplier;
import org.jage.agent.IAgent;
import org.jage.communication.message.IMessage;
import org.jage.communication.message.Messages;
import org.jage.property.AbstractPropertyContainer;
import org.jage.property.Property;
import org.jage.query.AgentEnvironmentQuery;
import org.jage.query.IValueFilter;
import org.jage.workplace.ConnectedSimpleWorkplace;
import org.jage.workplace.IWorkplaceEnvironment;
import org.jage.workplace.Workplace;
import org.jage.workplace.manager.WorkplaceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.toik.automaton.board.BoardFragment;
import pl.edu.agh.toik.automaton.board.Direction;
import pl.edu.agh.toik.automaton.board.Simple2DBooleanMatrix;
import pl.edu.agh.toik.automaton.jage.AddressingUtils;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.beust.jcommander.internal.Lists.newArrayList;
import static com.google.common.collect.Iterables.consumingIterable;

/**
 * Agent that have fragment of the board and carry out simulation
 */
public class BoardFragmentOwningAgent extends ConnectedSimpleWorkplace {

    private static Logger logger = LoggerFactory.getLogger(BoardFragmentOwningAgent.class);

    private static final long serialVersionUID = 9146385144435844359L;

    private final Configuration configuration;
    private final BoardFragment boardFragment;
    private final List<MessageReceiver> receivers;
    private final Map<String, AntDescriptor> descriptors = new HashMap<>();

    @Inject
    public BoardFragmentOwningAgent(Configuration configuration, AgentAddressSupplier supplier) {
        super(supplier);
        this.configuration = configuration;

        logger.info("Starting board fragment owning agent with parameters: \n" + configuration);

        boardFragment = new BoardFragment(configuration.getBoardSize(), configuration.getBoardSize(), configuration.getBorderSize());

        receivers = ImmutableList.<MessageReceiver>builder()
                .add(new AntDescriptorReceiver())
                .add(new BorderLandReceiver())
                .build();
    }

    @Override
    public void step() {
        // execute agents
        super.step();

        if (configuration.getNumberOfIterations() >= getStep()) {
            doStep();
        }
    }

    private void doStep() {
        System.out.println(getAddress().getFriendlyName() + " " + getStep());

        receiveMessages();

        if (getStep() % configuration.getSynchronizationFrequency() == 0) {
            sendSynchronizationData();
        }
        if (getStep() % configuration.getVisualisationFrequency() == 0) {
            sendDataToVisualisingAgent();
        }
    }

    private void sendDataToVisualisingAgent() {
        DataForVisualisation data = new DataForVisualisation(boardFragment.getCentralPart(), newArrayList(descriptors.values()), getStep(), configuration.getBorderSize());

        AgentAddress visualisingAgentAddress = AddressingUtils.resolveVisualisingAgentAddress(getWorkplaceEnvironment());
        IMessage<AgentAddress, DataForVisualisation> messageToSend = Messages.newUnicastMessage(getAddress(), visualisingAgentAddress, data);

        sendMessage(messageToSend);
    }

    private void sendSynchronizationData() {
        logger.debug("Starting synchronization");
        for (Direction direction : Direction.values()) {
            sendBorderToNeighbour(getAddressOfFragmentAt(direction.getOpposite()), direction);
        }
        logger.debug("Synchronization finished");
    }

    public AntAggregate getAggregateAt(Direction direction) {
        Coordinates fragmentCoordinates = direction.getNeighbourCoordinates(new Coordinates(column(), row()));

        if (0 <= fragmentCoordinates.getY() && fragmentCoordinates.getY() < configuration.getBoardSize() && 0 <= fragmentCoordinates.getX() && fragmentCoordinates.getX() < configuration.getBoardSize()) {
            int index = fragmentCoordinates.getY() * configuration.getFragmentsInRow() + fragmentCoordinates.getX();
            return AddressingUtils.resolveAggregate(getWorkplaceEnvironment(), index).orNull();
        }

        return null;
    }

    public AgentAddress getAddressOfAggregateAt(Direction direction) {
        Coordinates fragmentCoordinates = direction.getNeighbourCoordinates(new Coordinates(column(), row()));

        if (0 <= fragmentCoordinates.getY() && fragmentCoordinates.getY() < configuration.getBoardSize() && 0 <= fragmentCoordinates.getX() && fragmentCoordinates.getX() < configuration.getBoardSize()) {
            int index = fragmentCoordinates.getY() * configuration.getFragmentsInRow() + fragmentCoordinates.getX();
            return AddressingUtils.resolveAggregateAddress(getWorkplaceEnvironment(), index);
        }

        return null;
    }

    public AgentAddress getAddressOfFragmentAt(Direction direction) {
        Coordinates neighbourCoordinates = direction.getNeighbourCoordinates(new Coordinates(column(), row()));

        if (0 <= neighbourCoordinates.getY() && neighbourCoordinates.getY() < boardFragment.getHeight() && 0 <= neighbourCoordinates.getX() && neighbourCoordinates.getX() < boardFragment.getWidth()) {
            int index = neighbourCoordinates.getY() * configuration.getFragmentsInRow() + neighbourCoordinates.getX();
            return AddressingUtils.resolveFragmentAddress(getWorkplaceEnvironment(), index);
        }

        return null;
    }

    private void sendBorderToNeighbour(AgentAddress neighbourAddress, Direction direction) {
        if (neighbourAddress == null) {
            return;
        }
        Simple2DBooleanMatrix border = boardFragment.getBorder(direction);
        logger.info("About to send synchronization data to: " + neighbourAddress + " from: " + getAddress() + ", data: " + border);
        IMessage<AgentAddress, Simple2DBooleanMatrix> messageToSend = Messages.newUnicastMessage(getAddress(), neighbourAddress, border);
        sendMessage(messageToSend);
    }

    // -------------------------------------------------------------------------

    @SuppressWarnings("unchecked")
    private void receiveMessages() {
        for (IMessage<AgentAddress, ?> message : consumingIterable(getMessages())) {
            for (MessageReceiver receiver : receivers) {
                if (receiver.canHandle(message)) {
                    receiver.receive(message.getHeader().getSenderAddress(), getStep(), message.getPayload());
                }
            }
        }
    }

    private class AntDescriptorReceiver implements MessageReceiver<AntDescriptor> {

        @Override
        public void receive(AgentAddress senderAddress, long step, AntDescriptor antDescriptor) {
            descriptors.put(senderAddress.getFriendlyName(), antDescriptor);
        }

        @Override
        public boolean canHandle(IMessage message) {
            return message.getPayload() instanceof AntDescriptor;
        }
    }

    private class BorderLandReceiver implements MessageReceiver<Simple2DBooleanMatrix> {

        @Override
        public void receive(AgentAddress senderAddress, long step, Simple2DBooleanMatrix payload) {
            int senderIndex = fragmentIndex(senderAddress);
            int row = senderIndex / configuration.getFragmentsInRow();
            int col = senderIndex % configuration.getFragmentsInRow();

            Direction direction = relativePositionOfOtherFragment(col, row);

            logger.debug("Updating borderland, fragment: " + fragmentIndex() + ", from: " + fragmentIndex(senderAddress) + ", row: " + row + ", col: " + col + ", direction: " + direction + ", matrix: " + payload);

            boardFragment.updateBorderLand(direction, payload);
        }

        @Override
        public boolean canHandle(IMessage message) {
            return message.getPayload() instanceof Simple2DBooleanMatrix;
        }
    }

    private int column() {
        return fragmentIndex() % configuration.getFragmentsInRow();
    }

    private int row() {
        return fragmentIndex() / configuration.getFragmentsInRow();
    }

    private int fragmentIndex() {
        return fragmentIndex(getAddress());
    }

    private int fragmentIndex(AgentAddress fragmentAddress) {
        Matcher matcher = Pattern.compile("boardFragment\\-(\\d+)").matcher(fragmentAddress.getFriendlyName());
        if (!matcher.matches())
            throw new IllegalStateException();
        return Integer.parseInt(matcher.group(1));
    }

    private Direction relativePositionOfOtherFragment(int otherFragmentColumn, int otherFragmentRow) {
        int column = column();
        int row = row();

        if (column == otherFragmentColumn) {
            if (row == otherFragmentRow + 1)
                return Direction.N;
            return Direction.S;
        } else if (column == otherFragmentColumn + 1) {
            if (row == otherFragmentRow + 1)
                return Direction.NW;
            else if (row == otherFragmentRow - 1)
                return Direction.SW;
            else
                return Direction.W;
        } else {
            if (row == otherFragmentRow + 1)
                return Direction.NE;
            else if (row == otherFragmentRow - 1)
                return Direction.SE;
            else
                return Direction.E;
        }
    }

    public BoardFragment getBoardFragment() {
        return boardFragment;
    }

}
