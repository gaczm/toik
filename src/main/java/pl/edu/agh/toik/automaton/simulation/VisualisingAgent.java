package pl.edu.agh.toik.automaton.simulation;

import com.google.common.collect.ArrayListMultimap;
import org.jage.address.agent.AgentAddress;
import org.jage.address.agent.AgentAddressSupplier;
import org.jage.communication.message.IMessage;
import org.jage.platform.component.exception.ComponentException;
import org.jage.workplace.ConnectedSimpleWorkplace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.toik.automaton.board.Simple2DBooleanMatrix;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.collect.Iterables.consumingIterable;
import static java.lang.Math.max;

public class VisualisingAgent extends ConnectedSimpleWorkplace {

    private static final long serialVersionUID = -733203545344202698L;

    private static Logger logger = LoggerFactory.getLogger(VisualisingAgent.class);

    private final Configuration configuration;
    private final Path outputDirectory;
    private final List<String> stats = new LinkedList<>();
    private final List<DataForVisualisation> fragments;
    private final ArrayListMultimap<Integer, DataForVisualisation> dataForVisualisation = ArrayListMultimap.create();

    @Inject
    public VisualisingAgent(Configuration configuration, AgentAddressSupplier supplier) throws IOException {
        super(supplier);
        this.configuration = configuration;

        logger.info("Starting visualising agent");
        outputDirectory = prepareOutputDirectory();

        fragments = new ArrayList<>(configuration.getNumberOfFragments());
        for (int i = 0; i < configuration.getNumberOfFragments(); ++i) {
            fragments.add(new DataForVisualisation(new Simple2DBooleanMatrix(configuration.getBoardSize(), configuration.getBoardSize()), Collections.<AntDescriptor>emptyList(), 0, configuration.getBorderSize()));
        }
    }

    private Path prepareOutputDirectory() throws IOException {
        String name = "output";
        int maxFound = 0;
        try {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get("."), "output-*")) {
                for (Path path : stream) {
                    maxFound = max(maxFound, Integer.parseInt(path.getFileName().toString().replace("output-", "")));
                }
            }
        } catch (IOException e) {
            logger.warn("Cannot list current directory", e);
        }
        name = "output-" + ++maxFound;

        Path path = Paths.get(".", name);
        Files.createDirectory(path);
        return path;
    }

    @Override
    public void step() {
        super.step();
        logger.info("Step: " + getStep());

        for (final IMessage<AgentAddress, ?> message : consumingIterable(getMessages())) {
            if (message.getPayload() instanceof DataForVisualisation) {
                int index = fragmentIndex(message.getHeader().getSenderAddress());
                DataForVisualisation data = (DataForVisualisation) message.getPayload();
                logger.info("received message from fragment: " + index + ", " + fragments);
                dataForVisualisation.put(index, data);
            }
        }
    }

    @Override
    public boolean finish() throws ComponentException {
        logger.info("Simulation finished, preparing visualisation");

        try {
            int steps = dataForVisualisation.get(0).size();

            List<DataForVisualisation> fragments = new ArrayList<>(configuration.getNumberOfFragments());
            for (int i = 0; i < configuration.getNumberOfFragments(); ++i) {
                fragments.add(null);
            }
            for (int s = 0; s < steps; ++s) {
                for (int i = 0; i < configuration.getNumberOfFragments(); ++i) {
                    List<DataForVisualisation> dataForVisualisations = dataForVisualisation.get(i);
                    System.out.println("size: " + dataForVisualisations.size());
                    System.out.println("s: " + s);
                    DataForVisualisation element = dataForVisualisations.get(s);
                    fragments.set(i, element);
                }

                VisualisationUtils.drawBoardTo(fragments, configuration.getFragmentsInRow(), outputDirectory.resolve(s + ".png"));
                StringBuilder sb = new StringBuilder();

                sb.append("<ul>");
                for (int i = 0; i < fragments.size(); i++) {
                    DataForVisualisation fragment = fragments.get(i);
                    int column = i % configuration.getFragmentsInRow();
                    int row = i / configuration.getFragmentsInRow();
                    sb.append(String.format("<li>Row: %d, column: %d, step: %d, ants: %d</li>", row, column, fragment.getStep(), fragment.getAnts().size()));
                }
                sb.append("</ul>");

                stats.add(sb.toString());
            }
        } catch (IOException e) {
            logger.warn("Cannot write on of images", e);
        }
        VisualisationUtils.generatePage(stats, outputDirectory.resolve("index.html"));

        return super.finish();
    }

    private int fragmentIndex(AgentAddress fragmentAddress) {
        Matcher matcher = Pattern.compile("boardFragment\\-(\\d+)").matcher(fragmentAddress.getFriendlyName());
        if (!matcher.matches())
            throw new IllegalStateException();
        return Integer.parseInt(matcher.group(1));
    }
}
