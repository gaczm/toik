package pl.edu.agh.toik.automaton.jage;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import org.jage.address.agent.AgentAddress;
import org.jage.agent.IAgent;
import org.jage.property.Property;
import org.jage.workplace.IWorkplaceEnvironment;
import org.jage.workplace.Workplace;
import org.jage.workplace.manager.WorkplaceManager;
import pl.edu.agh.toik.automaton.simulation.AntAggregate;
import pl.edu.agh.toik.automaton.simulation.BoardFragmentOwningAgent;

import javax.annotation.Nullable;
import java.util.List;

public class AddressingUtils {

    public static AgentAddress resolveVisualisingAgentAddress(final IWorkplaceEnvironment environment) {
        return resolveAddress(environment, "visualisingAgent");
    }

    public static AgentAddress resolveFragmentAddress(final IWorkplaceEnvironment environment, final int index) {
        return resolveAddress(environment, "boardFragment-" + index);
    }

    public static AgentAddress resolveAggregateAddress(final IWorkplaceEnvironment environment, final int index) {
        return resolveAggregate(environment, index).transform(new Function<AntAggregate, AgentAddress>() {
            @Nullable
            @Override
            public AgentAddress apply(@Nullable AntAggregate input) {
                return input.getAddress();
            }
        }).orNull();
    }

    public static Optional<AntAggregate> resolveAggregate(final IWorkplaceEnvironment environment, final int index) {
        if (environment instanceof WorkplaceManager) {
            return FluentIterable.from(((WorkplaceManager) environment).getWorkplaces())
                    .firstMatch(new Predicate<Workplace<IAgent>>() {
                        @Override
                        public boolean apply(Workplace<IAgent> workplace) {
                            return workplace.getAddress().getFriendlyName().equals("boardFragment-" + index);
                        }
                    }).transform(new Function<Workplace<IAgent>, BoardFragmentOwningAgent>() {
                        @Override
                        public BoardFragmentOwningAgent apply(@Nullable Workplace<IAgent> input) {
                            Object tmp = input;
                            return (BoardFragmentOwningAgent) tmp;
                        }
                    }).transform(cast(BoardFragmentOwningAgent.class))
                    .transform(new Function<BoardFragmentOwningAgent, AntAggregate>() {
                        @Override
                        public AntAggregate apply(@Nullable BoardFragmentOwningAgent input) {
                            Property agents = input.getProperty("agents");
                            if (agents.getValue() instanceof List) {
                                List listOfAgents = (List) agents.getValue();

                                if (listOfAgents.size() == 1 && listOfAgents.get(0) instanceof AntAggregate) {
                                    return (AntAggregate) listOfAgents.get(0);
                                }
                            }
                            throw new IllegalStateException("BoardFragmentOwningAgent without aggregate?");
                        }
                    });
        }
        throw new IllegalStateException("Cannot resolve agent address");
    }

    private static AgentAddress resolveAddress(IWorkplaceEnvironment environment, final String friendlyName) {
        if (environment instanceof WorkplaceManager) {
            return FluentIterable.from(((WorkplaceManager) environment).getWorkplaces())
                    .firstMatch(new Predicate<Workplace<IAgent>>() {
                        @Override
                        public boolean apply(Workplace<IAgent> workplace) {
                            return workplace.getAddress().getFriendlyName().equals(friendlyName);
                        }
                    }).transform(new Function<Workplace<IAgent>, AgentAddress>() {
                        @Override
                        public AgentAddress apply(Workplace<IAgent> workplace) {
                            return workplace.getAddress();
                        }
                    }).orNull();
        }
        throw new IllegalStateException("Cannot resolve agent address");
    }

    private static <B> Function<Object, B> cast(Class<B> to) {
        return new Function<Object, B>() {
            @SuppressWarnings("unchecked")
            @Override
            public B apply(@Nullable Object input) {
                return (B) input;
            }
        };
    }

}
