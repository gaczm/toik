package pl.edu.agh.toik.automaton.simulation;

import pl.edu.agh.toik.automaton.board.BoardFragment;

import java.util.List;

public interface Simulation {
    Coordinates doStep(Coordinates antCoordinates, BoardFragment boardFragment, List<Coordinates> otherAntsCoordinateses);
}
