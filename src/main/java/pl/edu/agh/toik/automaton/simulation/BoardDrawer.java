package pl.edu.agh.toik.automaton.simulation;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class BoardDrawer {

    private static final int WIDTH = 800;
    private static final Color TAKEN_CELL_COLOR = new Color(200, 200, 200);
    private static final Color LIVING_ANT_COLOR = new Color(31, 224, 70);
    private static final Color DEAD_ANT_COLOR = new Color(186, 80, 71);
    private static final Color MIGRATING_ANT_COLOR = new Color(234, 231, 78);
    private static final BasicStroke BORDER_LAND_STROKE = new BasicStroke(4.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[]{10.0f}, 0.0f);
    private static final BasicStroke BORDER_STROKE = new BasicStroke(4.0f);
    private static final Color BORDER_LAND_COLOR = new Color(255, 0, 0, 130);

    private final int width;
    private final int height;
    private final int cellSize;
    private final BufferedImage image;
    private final Graphics2D graphics;
    private final List<DataForVisualisation> fragments;
    private int inRow;
    private final int inColumn;

    public BoardDrawer(List<DataForVisualisation> fragments, int inRow) {
        this.inRow = inRow;
        this.inColumn = fragments.size() / inRow;

        int totalWidth = fragments.get(0).getBoard().getWidth() * inRow;
        int totalHeight = fragments.get(0).getBoard().getHeight() * inColumn;

        this.fragments = fragments;
        width = WIDTH;
        height = WIDTH * totalHeight / totalWidth;
        cellSize = width / totalWidth;
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        graphics = image.createGraphics();
    }

    public void drawTo(File output) throws IOException {
        clearWholeArea();

        for (int i = 0; i < fragments.size(); i++) {
            DataForVisualisation fragment = fragments.get(i);
            int offsetX = (i % inRow) * (width / inRow);
            int offsetY = (i / inRow) * (height / inColumn);

            drawCellsState(offsetX, offsetY, fragment);
            drawAnts(offsetX, offsetY, fragment);
        }

        drawBorderLands();
        drawBordersBetweenFragments();

        ImageIO.write(image, "PNG", output);
    }

    private void clearWholeArea() {
        graphics.setPaint(Color.white);
        graphics.fillRect(0, 0, width, height);
    }

    private void drawCellsState(int offsetX, int offsetY, DataForVisualisation data) {
        graphics.setPaint(TAKEN_CELL_COLOR);
        for (int x = 0; x < data.getBoard().getWidth(); ++x) {
            for (int y = 0; y < data.getBoard().getHeight(); ++y) {
                if (data.getBoard().get(x, y)) {
                    fillCell(offsetX, offsetY, x, y);
                }
            }
        }
    }

    private void drawAnts(int offsetX, int offsetY, DataForVisualisation data) {
        for (AntDescriptor ant : data.getAnts()) {
            switch (ant.getState()) {
                case LIVING:
                    graphics.setPaint(LIVING_ANT_COLOR);
                    break;
                case DEAD:
                    graphics.setPaint(DEAD_ANT_COLOR);
                    break;
                case ABOUT_TO_MIGRATE:
                    graphics.setPaint(MIGRATING_ANT_COLOR);
                    break;
            }
            fillCell(offsetX, offsetY, ant.getCoordinates().getX(), ant.getCoordinates().getY());
        }
    }

    private void drawBorderLands() {
        int borderSize = fragments.get(0).getBorderSize();
        graphics.setPaint(BORDER_LAND_COLOR);
        graphics.setStroke(BORDER_LAND_STROKE);

        for (int i = 1; i < inRow; ++i) {
            int x = i * width / inRow;
            graphics.drawLine(x - borderSize * cellSize, 0, x - borderSize * cellSize, height);
            graphics.drawLine(x + borderSize * cellSize, 0, x + borderSize * cellSize, height);
        }
        for (int i = 1; i < inColumn; ++i) {
            int y = i * height / inColumn;
            graphics.drawLine(0, y - borderSize * cellSize, width, y - borderSize * cellSize);
            graphics.drawLine(0, y + borderSize * cellSize, width, y + borderSize * cellSize);
        }
    }

    private void drawBordersBetweenFragments() {
        graphics.setStroke(BORDER_STROKE);
        graphics.setPaint(Color.BLACK);
        for (int i = 0; i < inRow; ++i) {
            int x = i * width / inRow;
            graphics.drawLine(x, 0, x, height);
        }

        for (int i = 0; i < inColumn; ++i) {
            int y = i * height / inColumn;
            graphics.drawLine(0, y, width, y);
        }
    }

    private void fillCell(int offsetX, int offsetY, int positionX, int positionY) {
        graphics.fillRect(offsetX + positionX * cellSize, offsetY + positionY * cellSize, cellSize, cellSize);
    }

}
