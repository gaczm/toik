package pl.edu.agh.toik.automaton;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.math.IntMath;
import org.jage.platform.cli.CliNodeBootstrapper;

import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Path;

public class Bootstrapper {

    @Parameter(names = {"-f", "--fragments"}, description = "Number of fragments into which board will be divided")
    private int numberOfFragments = 4;

    @Parameter(names = {"-a", "--ants"}, description = "Number of ants per board fragment")
    private int numberOfAnts = 4;

    @Parameter(names = {"-i", "--iterations"}, description = "Number of iterations")
    private int numberOfIterations = 50;

    @Parameter(names = {"-s", "--size"}, description = "Board size")
    private int boardSize = 10;

    @Parameter(names = {"-b", "--border-size"}, description = "Size of border beeing synchronized")
    private int borderSize = 2;

    @Parameter(names = {"-q", "--syncfreq"}, description = "Synchronization frequency")
    private int synchronizationFrequency = 5;

    @Parameter(names = {"-h", "--help"}, help = true, description = "Shows this :-)")
    private Boolean help;

    public static void main(String[] args) {
        try {
            new Bootstrapper(args);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    public Bootstrapper(String... args) throws IOException {
        parseArguments(args);
        checkArguments();
        Path configFileLocation = AgeConfigurationGenerator.generateAgeConfigurationXML(mapWithConfiguration());
        configFileLocation.toFile().deleteOnExit();
        System.out.println("Bootstrapping simulation using parameters:");
        System.out.println(Joiner.on("\n").withKeyValueSeparator(": ").join(mapWithConfiguration()));
        System.out.println("Location of config file: " + configFileLocation);
        CliNodeBootstrapper.main(new String[]{"-Dage.node.conf=" + configFileLocation});
    }

    private void checkArguments() {
        int fragmentsInRow = IntMath.sqrt(numberOfFragments, RoundingMode.FLOOR);
        if (fragmentsInRow * fragmentsInRow != numberOfFragments) {
            System.err.println("Number of fragments must be second power of some number");
            System.exit(1);
        }
    }

    private ImmutableMap<String, Object> mapWithConfiguration() {
        return ImmutableMap.<String, Object>builder()
                .put("numberOfFragments", numberOfFragments)
                .put("numberOfAnts", numberOfAnts)
                .put("numberOfIterations", numberOfIterations)
                .put("boardSize", boardSize)
                .put("borderSize", borderSize)
                .put("synchronizationFrequency", synchronizationFrequency)
                .build();
    }

    private void parseArguments(String... args) {
        JCommander jCommander = new JCommander(this);

        try {
            jCommander.parse(args);
            if (help != null && help) {
                jCommander.usage();
                System.exit(0);
            }
        } catch (MissingCommandException e) {
            System.err.println("Unknown command: " + e.getMessage());
            jCommander.usage();
            System.exit(1);
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            jCommander.usage();
            System.exit(1);
        }
    }
}
