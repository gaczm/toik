package pl.edu.agh.toik.automaton.simulation;

import org.jage.action.AgentActions;
import org.jage.address.agent.AgentAddress;
import org.jage.address.agent.AgentAddressSupplier;
import org.jage.address.selector.agent.ParentAgentAddressSelector;
import org.jage.agent.AgentException;
import org.jage.agent.SimpleAgent;
import org.jage.communication.message.Header;
import org.jage.communication.message.IHeader;
import org.jage.communication.message.Message;
import org.jage.platform.component.exception.ComponentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.toik.automaton.board.BoardFragment;
import pl.edu.agh.toik.automaton.board.Direction;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Random;

public class AntAgent extends SimpleAgent implements Serializable {

    private static final long serialVersionUID = -4167840253172700756L;
    private static final Random random = new Random();

    private static Logger logger = LoggerFactory.getLogger(AntAgent.class);

    private Simulation simulation = new LangtonAntSimulator();
    private AntDescriptor antDescriptor;
    private AntAggregate parent = null;

    @Inject
    public AntAgent(AgentAddressSupplier supplier) {
        super(supplier);
        antDescriptor = new AntDescriptor(random.nextInt(10), random.nextInt(10), AntDescriptor.State.LIVING);
    }

    @Override
    public void step() {
        if (antDescriptor.getState().equals(AntDescriptor.State.LIVING)) {
            Coordinates coordinates = simulation.doStep(antDescriptor.getCoordinates(), getParent().getBoardFragment(), null);
            updateAntDescriptor(coordinates);

            try {
                IHeader<AgentAddress> header = new Header<>(getAddress(), new ParentAgentAddressSelector(getAddress()));
                Message<AgentAddress, AntDescriptor> message = new Message<>(header, antDescriptor);
                doAction(AgentActions.sendMessage(message));
            } catch (final AgentException e1) {
                logger.error("Could not send a message.", e1);
            }
        }
    }

    public void migrate(AntAggregate destination) {
        logger.info("Migrating to: " + destination);
        parent = destination;

        destination.add(this);

        // migrated
        // now update position
        int oldX = antDescriptor.getCoordinates().getX();
        int oldY = antDescriptor.getCoordinates().getY();
        int newX = antDescriptor.getCoordinates().getX();
        int newY = antDescriptor.getCoordinates().getY();
        int width = getParent().getBoardFragment().getWidth();
        int height = getParent().getBoardFragment().getHeight();

        if (oldX < 0) {
            newX += width;
        } else if (oldX >= width) {
            newX -= width;
        }
        if (oldY < 0) {
            newY += height;
        } else if (oldY >= height) {
            newY -= height;
        }
        antDescriptor = new AntDescriptor(newX, newY, AntDescriptor.State.LIVING);
        logger.info("Migrated");
    }

    private void updateAntDescriptor(Coordinates coordinates) {
        Direction direction = getDirectionForCoordinates(coordinates);
        if (direction != null) {
            AgentAddress addressOfFragmentAt = getParent().getAddressOfAggregateAt(direction);
            if (addressOfFragmentAt == null) {
                antDescriptor = antDescriptor.changeState(AntDescriptor.State.DEAD);
            } else {
                antDescriptor = new AntDescriptor(coordinates.getX(), coordinates.getY(), AntDescriptor.State.ABOUT_TO_MIGRATE);
            }
        } else {
            antDescriptor = new AntDescriptor(coordinates.getX(), coordinates.getY());
        }
    }

    private Direction getDirectionForCoordinates(Coordinates coordinates) {
        Direction direction = null;
        BoardFragment boardFragment = getParent().getBoardFragment();
        if (coordinates.getX() < 0) {
            if (coordinates.getY() < 0) {
                direction = Direction.NW;
            } else if (coordinates.getY() >= boardFragment.getHeight()) {
                direction = Direction.SW;
            } else {
                direction = Direction.W;
            }
        } else if (coordinates.getX() >= boardFragment.getWidth()) {
            if (coordinates.getY() < 0) {
                direction = Direction.NE;
            } else if (coordinates.getY() >= boardFragment.getHeight()) {
                direction = Direction.SE;
            } else {
                direction = Direction.E;
            }
        } else {
            if (coordinates.getY() < 0) {
                direction = Direction.N;
            } else if (coordinates.getY() >= boardFragment.getHeight()) {
                direction = Direction.S;
            }
        }
        return direction;
    }

    @Override
    public boolean finish() throws ComponentException {
        logger.info("Finishing {}", this);
        return super.finish();
    }

    @Override
    public String toString() {
        AgentAddress address = getAddress();
        if (address != null)
            return address.getFriendlyName();
        return "ant-?";
    }

    private AntAggregate getParent() {
        if (parent == null)
            return (AntAggregate) getAgentEnvironment();
        else
            return parent;
    }

    public AntDescriptor getAntDescriptor() {
        return antDescriptor;
    }
}
