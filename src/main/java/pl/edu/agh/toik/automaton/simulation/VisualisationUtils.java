package pl.edu.agh.toik.automaton.simulation;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import pl.edu.agh.toik.automaton.board.Simple2DBooleanMatrix;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static pl.edu.agh.toik.automaton.board.Simple2DBooleanMatrix.*;

public class VisualisationUtils {

    static public void main(String args[]) throws Exception {
        Simple2DBooleanMatrix matrix = matrix(10, 10,
                x, o, o, o, x, x, x, o, o, o,
                o, o, o, x, o, x, x, o, o, o,
                o, o, x, o, x, o, o, o, x, o,
                o, o, x, x, o, x, o, o, x, x,
                o, o, o, x, x, o, x, o, o, o,
                o, o, o, o, x, o, o, o, o, x,
                o, o, x, o, x, x, x, x, x, o,
                o, o, o, x, o, o, o, x, o, o,
                x, o, o, o, x, x, x, o, x, o,
                o, o, x, x, o, x, x, o, o, o
        );

        AntDescriptor ant1 = new AntDescriptor(4, 3, AntDescriptor.State.LIVING);
        AntDescriptor ant2 = new AntDescriptor(1, 0, AntDescriptor.State.DEAD);
        AntDescriptor ant3 = new AntDescriptor(6, 8, AntDescriptor.State.LIVING);
        AntDescriptor ant4 = new AntDescriptor(9, 2, AntDescriptor.State.ABOUT_TO_MIGRATE);

        DataForVisualisation data = new DataForVisualisation(matrix, asList(ant1, ant2, ant3, ant4), 5, 2);

        ArrayList<String> stats = new ArrayList<>(100);
        for (int i = 1; i <= 100; ++i) {
            drawBoardTo(asList(data, data, data, data, data, data, data, data, data), 3, Paths.get("/tmp/" + i + ".png"));
            stats.add("stats " + i);
        }
        generatePage(stats, Paths.get("/tmp/index.html"));
    }

    public static void drawBoardTo(List<DataForVisualisation> fragments, int fragmentsInRow, Path output) throws IOException {
        new BoardDrawer(fragments, fragmentsInRow).drawTo(output.toFile());
    }

    public static void generatePage(List<String> stats, Path output) {
        try {
            VelocityContext ctx = prepareContext(stats);
            String template = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("index.html.vm"));
            renderTemplate(template, ctx, output);
        } catch (IOException e) {
            throw new RuntimeException("Cannot load template", e);
        }
    }

    private static VelocityContext prepareContext(List<String> stats) {
        Velocity.init();
        VelocityContext ctx = new VelocityContext();
        ctx.put("max", stats.size());
        ctx.put("stats", stats);
        return ctx;
    }

    private static void renderTemplate(String template, VelocityContext ctx, Path output) throws IOException {
        try (Writer writer = Files.newBufferedWriter(output, Charset.forName("utf-8"))) {
            Velocity.evaluate(ctx, writer, "age.xml", template);
        }
    }
}
